# esay

#### 介绍
基于webmagic的通用爬虫抓取应用，核心在于简单易用，搭建好后轻松抓取数据

#### 前端代码
https://gitee.com/mountFuji/easy-crawl-front

#### 软件架构
软件架构说明
- 基于springboot实现
- 爬虫框架使用的是webmagic
- 数据库默认的是h2，配置文件在application.yml，如果想切换成mysql，请参考分支useMysql里的application.yml配置

#### 安装教程

1. EasyApplication 启动入口

#### 使用说明
- 基本流程
    1. EasyApplication 启动入口
    2. 打开 localhost:8080
    3. 新建爬虫(默认会导入两个爬虫示例)
    4. 运行
    5. 查看任务
- 支持爬虫的导入导出

#### 页面预览
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/d23ba7e8923d4ee58d853fe6491a8594.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/5b7a40d7f8854d8ea9cde55283d8ef26.jpg)
![avatar](http://xdd-1258215774.cos.ap-shanghai.myqcloud.com/d8ad6d69dd1741169680b37d255e079e.jpg)

#### 参与贡献
1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 最近完成功能
1.支持任务停止
2.支持任务暂停
3.支持任务暂停后恢复
4.系统重启时自动将进行中的任务恢复爬取
5.【new】支持正文页内容的爬取，之前只能爬取列表页的数据
6.支持配置爬虫的线程数，爬取页面后的休眠时间等配置信息

#### 近期待完成功能
1.完善创建爬虫的教程
2.待定 欢迎小伙伴们评论区提出需求

#### 待修复的bug
1.爬取的内容里有特殊字符 如 ' , 存入数据库的时候报错

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)